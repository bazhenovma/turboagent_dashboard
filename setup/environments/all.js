'use strict';

/**
 * Module dependencies.
 */
var log                 = require('application/dependencies/log')(module),
    passport            = require('passport'),
    bodyParser          = require('body-parser'),
    favicon             = require('static-favicon'),
    domain              = require('domain'),
    config              = require('application/dependencies/config'),
    routes              = require('application/routes/')(),
    connect             = require('connect');

module.exports = function() {
    this.use(favicon(__dirname + '/../../../frontend/static/favicon.ico'));

    if (this.get('env') == 'development') {
        this.use(connect.logger('dev'));
    } else {
        this.use(connect.logger('default'));
    }
    this.use(bodyParser());
    this.use(passport.initialize());

    this.use(function(req, res, next) {

        var d = domain.create();

        d.on('error', function(err) {
            if([403,404,406].indexOf(err.status) != -1) {
                res.json(err.status, err.message);
            } else {
                console.error(err.stack);
                res.statusCode = 500;
                res.setHeader('content-type', 'text/plain');
                res.end('Houston, we have a problem!\n');
                domain.active.emit('error', err);
            }
        });

        d.add(req);
        d.add(res);

        d.run(function () {
            res.setHeader('Access-Control-Allow-Origin', config.get('allowedDomains'));
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-user-id,x-user-token');
            res.setHeader('Access-Control-Allow-Credentials', true);
            res.charset = 'utf-8';

            if (req.method == 'OPTIONS') {
                res.send(200);
            } else {
                next();
            }
        });
    });

    routes.init(this);
};