var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    user_API    = require('application/core/api/user');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
},user_API.authorization));