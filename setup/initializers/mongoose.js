var mongoose = require('mongoose'),
    config = require('application/dependencies/config'),
    log = require('application/dependencies/log')(module),
    autoIncrement = require('mongoose-auto-increment');

mongoose.connection.on('connected', function(){
    log.info('Database connected');
    var collection = mongoose.connection.db.collection('addresses');
    collection.ensureIndex({shortAddress: 'text'}, { default_language: "russian" },function(){});
});

mongoose.connection.on('error', function(err){
    log.error(err);
});

mongoose.connection.on('disconnected', function() {
    log.info('Database disconnected');
    process.exit(0);
});

mongoose.connect(config.get('mongoose:uri'), config.get('mongoose:options'));

autoIncrement.initialize(mongoose.connection);

module.exports = mongoose;