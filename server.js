'use strict';

/**
 * Module dependencies.
 */

var serverDomain   = require('domain').create(),
    fs             = require('fs'),
    log            = require('application/dependencies/log')(module);

serverDomain.on('error', function(err){
    console.error(err.stack);
    log.error(err.message);
});

serverDomain.run(function(){
    var config         = require('application/dependencies/config'),
        express        = require('express'),
        bootable       = require('bootable'),
        bootableEnv    = require('bootable-environment'),
        https          = require('https'),
        http           = require('http');

    /*var options = {
        key: fs.readFileSync('../keys/agent2-key.pem'),
        cert: fs.readFileSync('../keys/agent2-cert.pem')
    };*/
        var app = bootable(express());
        // Setup initializers, environments, routes
        app
            .phase(bootable.initializers('./setup/initializers/'))
            .phase(bootableEnv('./setup/environments/', app));

        // Boot app
        app.boot(function(err) {
            if (err) { throw new Error(err); }
            http.createServer(app).listen(config.get('port'));
            //https.createServer(options, app).listen(443);
            log.info('Express listen port', config.get('port'));
        });
});


