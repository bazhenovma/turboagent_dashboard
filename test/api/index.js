'use strict';

var geoLocation = require('./geoLocation');

module.exports = (function(){

    return {
        runTests: function() {
            geoLocation.runTests();
        }
    }
})();