'use strict';

var geoLocation_api = new require('application/core/api/geolocation/')().init(),
    country_api = geoLocation_api.country,
    area_api = geoLocation_api.area,
    town_api = geoLocation_api.town,
    street_api = geoLocation_api.street,
    district_api = geoLocation_api.district,
    address_api = geoLocation_api.address,
    expect = require("chai").expect,
    async = require('async');

module.exports = (function () {
    return {
        runTests: function () {

            describe("Geo API", function () {
                describe("Create full address", function () {
                    var country, area, town, district, street, address;

                    it('Create and update country', function (done) {
                        country_api.create({name: 'USA'}, function (err, result) {
                            expect(err).not.to.be.null;
                        });
                        country_api.create({name: 'Россия'}, function (err, result) {
                            expect(err).to.be.null;
                            expect(result).not.to.be.null;
                            country = result;
                            done();
                        });
                    });
                    it('Create and update area', function (done) {
                        area_api.create({name: 'Красноярский край', _country: country._id}, function (err, result) {
                            expect(err).to.be.null;
                            expect(result).not.to.be.null;
                            area = result;
                            done();
                        });
                    });
                    it('Create and update town', function (done) {
                        town_api.create({name: 'Красноярск', _area: area}, function (err, result) {
                            expect(err).to.be.null;
                            expect(result).not.to.be.null;
                            town = result;
                            done();
                        });
                    });
                    it('Create and update district', function (done) {
                        district_api.create({name: 'Октябрьский район', _town: town}, function (err, result) {
                            expect(err).to.be.null;
                            expect(result).not.to.be.null;
                            district = result;
                            done();
                        });
                    });
                    it('Create and update street', function (done) {
                        street_api.create({name: 'Академика Киренского', _town: town, _district: district}, function (err, result) {
                            expect(err).to.be.null;
                            expect(result).not.to.be.null;
                            street = result;
                            done();
                        });
                    });
                    it('Create and update address', function (done) {
                        address_api.create({house: '5', _street: street, _town: town, _district: district}, function (err, result) {
                            expect(err).to.be.null;
                            expect(result).not.to.be.null;
                            address = result;

                            done();
                        });
                    });
                    it('Remove added info', function (done) {
                        country_api.remove(country._id, done);
                    });
                });
            });
        }
    }
})();