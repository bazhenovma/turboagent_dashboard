var path = require('path'),
    util = require('util'),
    http = require('http'),
    t = require('application/dependencies/translate/');
// ошибки для выдачи посетителю
function HttpError(message,status,errors) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, HttpError);

    this.status = status;
    this.message = { text: t.__(message || http.STATUS_CODES[status] || "Error"), errors: errors ? errors : null };
}

util.inherits(HttpError, Error);

HttpError.prototype.name = 'HttpError';

module.exports = HttpError;