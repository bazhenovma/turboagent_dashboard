'use strict';

var winston = require('winston'),
    ENV = process.env.NODE_ENV,
    path = require('path');

function getLogger(module) {

    var label = module.filename.split('\\').slice('-2').join('\\'),
        level = ENV == 'development' ? 'debug' : 'error';

    return new winston.Logger({
        transports: [
            new winston.transports.Console({
                colorize: true,
                level: level,
                label: label
            }),
            new winston.transports.File({
                colorize: true,
                level: level,
                label: label,
                filename: path.join(__dirname,'/../../../../app/logs/dashboard-app_'+level+'.log'),
                maxFiles: 5,
                maxsize: 6666666
            })
        ]
    })
}

module.exports = getLogger;