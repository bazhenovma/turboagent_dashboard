var t = require('i18n');

t.configure({
    locales:['ru','en'],
    directory: __dirname + '/locales',
    defaultLocale: 'ru'
});

module.exports = t;