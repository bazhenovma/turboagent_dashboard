var mongoose = require('setup/initializers/mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    async = require('async'),
    _ = require('underscore');

var geoPointSchema = new Schema({
    point: {
        type: [Number],
        index: '2d'
    }
});

var countrySchema = new Schema({
    _geopoint: {
        type:Schema.Types.ObjectId,
        ref: 'geoPoint'
    },
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    currency: {
        cod: {
            type: String
        },
        name: {
            type: String
        }
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    }
},{ autoIndex: true });

var areaScheme = new Schema({
    _geopoint: {
        type:Schema.Types.ObjectId,
        ref: 'geoPoint',
        required: true
    },
    _country: {
        type:Schema.Types.ObjectId,
        ref: 'Country',
        required: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    typeLongName: {
        type: String
    },
    typeShortName: {
        type: String
    },
    kladr_ID: {
        type: Number
    },
    okato_ID: {
        type: Number
    },
    updated: {
        type: Date
    }
},{ autoIndex: true });

var townSchema = new Schema({
    _geopoint: {
        type:Schema.Types.ObjectId,
        ref: 'geoPoint',
        required: true
    },
    _area: {
        type:Schema.Types.ObjectId,
        ref: 'Area',
        required: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    typeLongName: {
        type: String
    },
    typeShortName: {
        type: String
    },
    kladr_ID: {
        type: Number
    },
    okato_ID: {
        type: Number
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    }
},{ autoIndex: true });

var streetSchema = new Schema({
    _geopoint: {
        type:Schema.Types.ObjectId,
        ref: 'geoPoint',
        required: true
    },
    _town: {
        type:Schema.Types.ObjectId,
        ref: 'Town',
        required: true
    },
    _district: {
        type:Schema.Types.ObjectId,
        ref: 'District'
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    typeLongName: {
        type: String
    },
    typeShortName: {
        type: String
    },
    kladr_ID: {
        type: Number
    },
    okato_ID: {
        type: Number
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    }
},{ autoIndex: true });

var districtSchema = new Schema({
    _geopoint: {
        type:Schema.Types.ObjectId,
        ref: 'geoPoint',
        required: true
    },
    _town: {
        type:Schema.Types.ObjectId,
        ref: 'Town',
        required: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    }
},{ autoIndex: true });

var addressSchema = new Schema({
    _geopoint: {
        type:Schema.Types.ObjectId,
        ref: 'geoPoint',
        required: true
    },
    _district: {
        type:Schema.Types.ObjectId,
        ref: 'District'
    },
    _street: {
        type:Schema.Types.ObjectId,
        ref: 'Street',
        required: true
    },
    _town: {
        type:Schema.Types.ObjectId,
        "ref": 'Town',
        required: true
    },
    typeLongName: {
        type: String
    },
    typeShortName: {
        type: String
    },
    kladr_ID: {
        type: Number
    },
    okato_ID: {
        type: Number
    },
    fullAddress: {
        type: String
    },
    shortAddress: {
        type: String
    },
    name: {
        type: String,
        trim: true
    },
    house: {
        type: String
    },
    housing: { //Корпус
        type: String
    },
    zip: {
        type: Number
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    }
},{ autoIndex: true });

function updatedHandler(next) {
    if(this.isNew)
        return next();
    else
        this.updated = Date.now();
    return next();
}

addressSchema.pre('save',updatedHandler);
districtSchema.pre('save',updatedHandler);
streetSchema.pre('save',updatedHandler);
townSchema.pre('save',updatedHandler);
areaScheme.pre('save',updatedHandler);
countrySchema.pre('save',updatedHandler);




/**
 * Удаляем все зависимости
 */

districtSchema.pre('remove',function(next) {
    mongoose.model('Address', addressSchema).find({_district: this._id},function(err,results){
        async.each(results,function(item,callback){
            item.remove(callback);
        },function(err){
            next();
        });
    });
});

streetSchema.pre('remove',function(next) {
    mongoose.model('Address', addressSchema).find({_street: this._id},function(err,results){
        async.each(results,function(item,callback){
            item.remove(callback);
        },function(err){
            next();
        });
    });
});

townSchema.pre('remove',function(next) {
    var model = this;
    async.waterfall([
        function(callback) {
            mongoose.model('District', districtSchema).find({_town: model._id},function(err,results){
                async.each(results,function(item,callback){
                    item.remove(callback);
                },function(err){
                    if(err)
                        throw new Error(err);

                    callback();
                });
            });
        },
        function(callback) {
            mongoose.model('Street', streetSchema).find({_town: model._id},function(err,results){
                async.each(results,function(item,callback){
                    item.remove(callback);
                },function(err){
                    if(err)
                        throw new Error(err);

                    callback();
                });
            });
        }
    ],function(err,result){
        if(err)
            throw new Error(err);
        next();
    })


});

areaScheme.pre('remove',function(next) {
    mongoose.model('Town', townSchema).find({_area: this._id},function(err,results){
        async.each(results,function(item,callback){
            item.remove(callback);
        },function(err){
            next();
        });
    });
});

countrySchema.pre('remove',function(next) {
    mongoose.model('Area', areaScheme).find({_country: this._id},function(err,results){
        async.each(results,function(item,callback){
            item.remove(callback);
        },function(err){
            next();
        });
    });
});

countrySchema.plugin(autoIncrement.plugin, { model: 'Country', field: 'ID',startAt: 1 });
townSchema.plugin(autoIncrement.plugin, { model: 'Town', field: 'ID',startAt: 1 });
areaScheme.plugin(autoIncrement.plugin, { model: 'Area', field: 'ID',startAt: 1 });
districtSchema.plugin(autoIncrement.plugin, { model: 'District', field: 'ID',startAt: 1 });
streetSchema.plugin(autoIncrement.plugin, { model: 'Street', field: 'ID',startAt: 1 });
addressSchema.plugin(autoIncrement.plugin, { model: 'Address', field: 'ID',startAt: 1 });

var country = mongoose.model('Country', countrySchema);

exports.Address = mongoose.model('Address', addressSchema);
exports.District = mongoose.model('District', districtSchema);
exports.Street = mongoose.model('Street', streetSchema);
exports.Town = mongoose.model('Town', townSchema);
exports.Area = mongoose.model('Area', areaScheme);
exports.Country = country;
exports.geoPoint = mongoose.model('geoPoint', geoPointSchema);


/**
 * Проверяем унивкальность названия
 */
countrySchema.path('name').validate(function (value, respond) {
    var current = this;
    country.findOne({name: value},function(err, country){
        if(err)
            throw err;
        if(country) {
            respond(country.ID == current.ID);
        } else
            respond(true);
    })

}, 'This field not unique');