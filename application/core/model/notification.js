var mongoose = require('setup/initializers/mongoose'),
    Schema = mongoose.Schema;

var notificationSchema = new Schema({
    type: {
        type: Number //Email, телефон
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    },
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    _event: {
        type: Schema.Types.ObjectId,
        ref: 'notificationEvent'
    }
});

var notificationEventSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    description: {
        type: String,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    }
});

function updatedHandler(next) {
    if(this.isNew)
        return next();
    else
        this.updated = Date.now();

    return next();
}

notificationSchema.pre('save',updatedHandler);
notificationEventSchema.pre('save',updatedHandler);

exports.Notification = mongoose.model('Notification', notificationSchema);
exports.notificationEvent = mongoose.model('notificationEvent', notificationEventSchema);