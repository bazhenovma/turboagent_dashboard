var mongoose = require('setup/initializers/mongoose'),
    Schema = mongoose.Schema;

var imageSchema = new Schema({
    src: {
        type: String
    },
    title: {
        type: String,
        trim: true
    },
    description: {
        type: String,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    },
    settings: {
        type: Schema.Types.Mixed
    },
    _uploader: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    _comments: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    _likes: [{
        type: Schema.Types.ObjectId,
        ref: 'Like'
    }]
});

function updatedHandler(next) {
    if(this.isNew)
        return next();
    else
        this.updated = Date.now();

    return next();
}

imageSchema.pre('save',updatedHandler);

exports.Image = mongoose.model('Image', imageSchema);