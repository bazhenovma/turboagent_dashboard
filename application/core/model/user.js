var mongoose = require('setup/initializers/mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto'),
    jwt = require('jwt-simple'),
    tokenSecret = ')*(F&G(*f7dg089fnb897g6*F&(GF',
    autoIncrement = require('mongoose-auto-increment');

var userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    status: {
        code: {
            type: Number
        },
        description: {
            type: String,
            trim: true
        },
        _codeChanger: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    info: {
        firstName: {
            type: String,
            trim: true
        },
        fullName: {
            type: String,
            trim: true
        },
        patronymic: {
            type: String,
            trim: true
        }
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date
    },
    settingsExtend: {
        type: Schema.Types.Mixed
    },
    purse: [{ //Кошелёк
        money: {
            type: Number
        },
        currency: {
            type: String
        }
    }],
    access: {
        role: {
            type: String
        },
        actions: [String]
    },
    attemptsToEnter:{
        type: Number,
        default: 0
    },
    _accounts: [{
        type: Schema.Types.ObjectId,
        ref: 'Account'
    }],
    _phones: [{
        type: Schema.Types.ObjectId,
        ref: 'Phone'
    }],
    _password: {
        type: Schema.Types.ObjectId,
        ref: 'Password'
    },
    _oldPasswords: [{
        type: Schema.Types.ObjectId,
        ref: 'Password'
    }],
    _notifications: [{
        type: Schema.Types.ObjectId,
        ref: 'Notification'
    }],
    _likedAdverts: [{
        type: Schema.Types.ObjectId,
        ref: 'Advert'
    }],
    _starsAdverts: [{
        type: Schema.Types.ObjectId,
        ref: 'Advert'
    }]
});

userSchema.methods.encodeToken = function(ip) {
    var date = Date.now();
    return jwt.encode({ id: this._id, expired: date + 1000 * 60 * 60 * 24 * 7, ip: ip}, tokenSecret);
}

var decodeToken = function(token) {
    return jwt.decode(token, tokenSecret);
}

var passwordSchema = new Schema({
    hash: {
        type: String
    },
    salt: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now()
    },
    disabled: {
        type: Date
    }
});

passwordSchema.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

passwordSchema.virtual('newPassword')
    .set(function(newPassword) {
        this.salt = Math.random() + '';
        this.hash = this.encryptPassword(newPassword);
    });

passwordSchema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hash;
};

var phoneSchema = new Schema({
    hash: {
        type: String
    },
    status: {
        code: {
            type: Number
        },
        description: {
            type: String,
            trim: true
        },
        _codeChanger: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    complaint: [{
        _user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        description: {
            type: String,
            trim: true
        },
        from: {
            type: String
        }
    }],
    _added: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date
    }
});

var accountSchema = new Schema({
    strategy: {
        type: String
    },
    token: {
        type: String
    },
    refreshToken: {
        type: String
    },
    profile: {
        type: Schema.Types.Mixed
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date
    }
});

function updatedHandler(next) {
    if(this.isNew)
        return next();
    else
        this.updated = Date.now();

    return next();
}

userSchema.pre('save',updatedHandler);
accountSchema.pre('save',updatedHandler);

phoneSchema.plugin(autoIncrement.plugin, { model: 'Phone', field: 'ID',startAt: 1 });
accountSchema.plugin(autoIncrement.plugin, { model: 'Account', field: 'ID',startAt: 1 });
passwordSchema.plugin(autoIncrement.plugin, { model: 'Password', field: 'ID',startAt: 1 });
userSchema.plugin(autoIncrement.plugin, { model: 'User', field: 'ID',startAt: 1 });

exports.Phone = mongoose.model('Phone', phoneSchema);
exports.Account = mongoose.model('Account', accountSchema);
exports.Password = mongoose.model('Password', passwordSchema);
exports.User = mongoose.model('User', userSchema);
exports.decodeToken = decodeToken;