var mongoose = require('setup/initializers/mongoose'),
    Schema = mongoose.Schema;

var commentSchema = new Schema({
    text: {
        type: String,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    },
    author: {
        IP: {
            type: String
        },
        userAgent: {
            type: String
        },
        email: {
            type: String
        },
        nickname: {
            type: String
        },
        _user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    _images: [{
        type: Schema.Types.ObjectId,
        ref: 'Image'
    }],
    _inResponseTo: {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    },
    _rating: {
        like: [{
            type: Schema.Types.ObjectId,
            ref: 'Like'
        }],
        dislike: [{
            type: Schema.Types.ObjectId,
            ref: 'Dislike'
        }]
    }
});

var likeSchema = new Schema({
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

var dislikeSchema = new Schema({
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

function updatedHandler(next) {
    if(this.isNew)
        return next();
    else
        this.updated = Date.now();

    return next();
}

commentSchema.pre('save',updatedHandler);

exports.Like = mongoose.model('Like', likeSchema);
exports.Dislike = mongoose.model('Dislike', dislikeSchema);
exports.Comment = mongoose.model('Comment', commentSchema);