var mongoose = require('setup/initializers/mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');

var advertSchema = new Schema({
    type: {
        type: Number,
        required: true
    },
    object_type: {
        type: Number,
        required: true
    },
    photo: {
        type: Boolean,
        default: 0
    },
    status: {
        code: {
            type: Number
        },
        description: {
            type: String
        },
        _codeChanger: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    optionsExtend: {
        type: Schema.Types.Mixed
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    },
    _author: {
        type:Schema.Types.ObjectId,
        ref: 'User'
    },
    _object: {
        type:Schema.Types.ObjectId,
        ref: 'Object',
        required: true
    },
    _house: {
        type: Schema.Types.ObjectId,
        ref: 'House'
    },
    _notifications: [{
        type: Schema.Types.ObjectId,
        ref: 'Notification'
    }],
    _comments: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    _likes: [{
        type: Schema.Types.ObjectId,
        ref: 'Like'
    }]
});

var objectSchema = new Schema({
    floor: {
        type: Number
    },
    repair: {
        type: Number
    },
    area: {
        gross: {
            type: Number
        },
        live: {
            type: Number
        },
        kitchen: {
            type: Number
        }
    },
    wc: {
        type: Number,
        default: 0
    },
    rooms: {
        type: Number
    },
    balkon: {
        type: Number,
        default: 0
    },
    price: {
        value: {
            type: Number
        },
        measure: {
            type: String
        }
    },
    description: {
        type: String,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    },
    gas: {
        type: Number,
        default: 0
    },
    _images: [{
        type:Schema.Types.ObjectId,
        ref: 'Image'
    }],
    _plan: [{
        type:Schema.Types.ObjectId,
        ref: 'Image'
    }],
    _house: {
        type: Schema.Types.ObjectId,
        ref: 'House'
    }
});

var houseSchema = new Schema({
    material: {
        type: Number
    },
    floors: {
        type: Number
    },
    type: {
        type: Number //Новостройка вторичка
    },
    gas: {
        type: Number,
        default: 0
    },
    built_year: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    },
    status: {
        code: {
            type: Number
        },
        description: {
            type: String
        },
        _codeChanger: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    _address: {
        type:Schema.Types.ObjectId,
        ref: 'Address'
    },
    _images: [{
        type:Schema.Types.ObjectId,
        ref: 'Image'
    }],
    _plan: [{
        type:Schema.Types.ObjectId,
        ref: 'Image'
    }],
    _author: {
        type:Schema.Types.ObjectId,
        ref: 'User'
    }
});

advertSchema.pre('save',updatedHandler);
objectSchema.pre('save',updatedHandler);
houseSchema.pre('save',updatedHandler);

advertSchema.plugin(autoIncrement.plugin, { model: 'Advert', field: 'ID',startAt: 1 });
objectSchema.plugin(autoIncrement.plugin, { model: 'Object', field: 'ID',startAt: 1 });
houseSchema.plugin(autoIncrement.plugin, { model: 'House', field: 'ID',startAt: 1 });

function updatedHandler(next) {
    if(this.isNew)
        return next();
    else
        this.updated = Date.now();

    return next();
}

exports.House = mongoose.model('House', houseSchema);
exports.Object = mongoose.model('Object', objectSchema);
exports.Advert = mongoose.model('Advert', advertSchema);