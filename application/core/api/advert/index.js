'use strict';

var async            = require('async'),
    Advert           = require('application/core/model/advert').Advert,
    eventEmitter     = require('events').EventEmitter,
    currentUser      = null,
    channel          = new eventEmitter(),
    _                = require('underscore');

var API_advert = (function(){

    var self = {
        allowedKeys : {
            create: ['type','object_type','photo','status','options','_author','_object','_house'],
            update: ['type','object_type','photo','status','options']
        },
        trimNotAllowedKeys: function(action,object) {
            var trimmedArray = {};

            for(var key in object) {
                if(self.allowedKeys[action].indexOf(key) != -1)
                    trimmedArray[key] = object[key];
                else {
                    //Посылаем сообщение для отлова взломщика
                    channel.emit('API_advert:notAllowedKey',{
                        action: action,
                        key: object[key]
                    });
                }
            }

            return trimmedArray;
        },
        create: function(object,callback) {
            var newAdvert = new Advert(object);
            newAdvert.save(function(err) {
                if(err)
                    throw new Error(err);

                callback(err, newAdvert);
            });
        }
    }

    //Public methods
    return {
        create: function(object,callback) {
            self.create(self.trimNotAllowedKeys('create',object),callback);
        },
        STATUS_CODE: {
            published: 1
        }
    }

})();

module.exports = API_advert;