'use strict';

var async            = require('async'),
    House            = require('application/core/model/advert').House,
    eventEmitter     = require('events').EventEmitter,
    currentUser      = null,
    channel          = new eventEmitter(),
    _                = require('underscore');

var API_house = (function(){

    var self = {
        allowedKeys : {
            create: ['material','floors','type','gas','built_year','_address'],
            update: ['material','floors','type','gas','built_year','_address']
        },
        trimNotAllowedKeys: function(action,object) {
            var trimmedArray = {};

            for(var key in object) {
                if(self.allowedKeys[action][key])
                    trimmedArray[key] = object[key];
                else {
                    //Посылаем сообщение для отлова взломщика
                    channel.emit('API_house:notAllowedKey',{
                        action: action,
                        key: object[key]
                    });
                }
            }

            return trimmedArray;
        },
        create: function(object,callback) {
            var newHouse = new House(object);
            newHouse.save(function(err) {
                if(err)
                    throw new Error(err);

                callback(err, newHouse);
            });
        }
    }

    //Public methods
    return {
        create: function(object,callback) {
            self.create(self.trimNotAllowedKeys('create',object),callback);
        },
        STATUS_CODE: {
            addedByUser: 1
        }
    }

})();

module.exports = API_house;