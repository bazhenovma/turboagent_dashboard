'use strict';

var nav_api = (function() {

    var admin_nav = [
        {
            'state': 'statistic',
            'name':'Статистика',
            'icon':'statistic'
        },
        {
            'state': 'settings',
            'name':'Настройки портала',
            'icon':'settings'
        },
        {
            'state': 'users',
            'name':'Пользователи',
            'icon':'users'
        },
        {
            'state': 'adverts',
            'name':'Объявления',
            'icon':'adverts'
        }
    ];

    return {
        getPrimaryNavForCurrentUser: function(req) {
            return admin_nav;
        }
    }
})();

module.exports = nav_api;