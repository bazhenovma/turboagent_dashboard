'use strict';

var async            = require('async'),
    _Object          = require('application/core/model/advert').Object,
    Advert           = require('application/core/model/advert').Advert,
    eventEmitter     = require('events').EventEmitter,
    currentUser      = null,
    channel          = new eventEmitter(),
    _                = require('underscore'),
    HttpError        = require('application/dependencies/HttpError');

var API_object = (function(){

    var self = {
        allowedKeys : {
            create: ['floor','repair','area','wc','rooms','balkon','price','description','_house'],
            update: ['floor','repair','area','wc','rooms','balkon','price','description']
        },
        trimNotAllowedKeys: function(action,object) {
            var trimmedArray = {};

            for(var key in object) {
                if(self.allowedKeys[action].indexOf(key) != -1)
                    trimmedArray[key] = object[key];
                else {
                    //Посылаем сообщение для отлова взломщика
                    channel.emit('API_object:notAllowedKey',{
                        action: action,
                        key: object[key]
                    });
                }
            }
            return trimmedArray;
        },
        create: function(object,callback) {
            var newObject = new _Object(object);
            newObject.save(function(err) {
                if(err)
                    throw new Error(err);

                callback(err, newObject);
            });
        },
        update: function(id,object,callback) {
            _Object.findOneAndUpdate({id: id},object,null,callback);
        },
        getByID: function(id,callback) {
            _Object.findById(id,callback);
        },
        pushImage: function(id,image,key,callback) {
            self.getByID(id,function(err,model){
                if(err) throw new Error(err);

                if(model) {
                    model[key].push(image);

                    //Выставляем объявлению флаг что к объекту приклепленны фото
                    self.getAttachedToAdverts(model.id,function(err,advert){
                        if(err) throw new Error(err);

                        if(advert) {
                            advert.photo = true;
                            advert.save(function(err){
                                if(err)
                                    throw new Error(err);
                            });
                        } else
                            throw new HttpError(500,'Object not attached to advert');
                    })
                    return model.save(callback);
                }
                throw new HttpError(404,'Object not found');
            });
        },
        getAttachedToAdvert: function(id,callback) {
            Advert.findOne({'_object': id},callback);
        }
    }

    //Public methods
    return {
        create: function(object,callback) {
            self.create(self.trimNotAllowedKeys('create',object),callback);
        },
        update: function(id,object,callback) {
            self.update(id,self.trimNotAllowedKeys('update',object),callback);
        },
        getByID: function(id,callback){
            self.getByID(id,callback);
        },
        attachImage: function(id, image, callback) {
            self.pushImage(id, image,'_images', callback);
        },
        attachPlan: function(id, image, callback) {
            self.pushImage(id, image,'_plan', callback);
        }
    }

})();

module.exports = API_object;