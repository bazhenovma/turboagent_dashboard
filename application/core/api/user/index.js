'use strict';

var Model           = require('application/core/model/user').User,
    PasswordModel   = require('application/core/model/user').Password,
    async           = require('async'),
    t               = require('application/dependencies/translate/'),
    HttpError       = require('application/dependencies/HttpError');

var API_user = (function(){

    var STATIC = {
        usersRoles: ['administrator','user','moderator','custom'],
        usersStatuses: [{
            id: 0,
            name: 'Активен'
        },{
            id: 1,
            name: 'Заблокирован'
        },{
            id: 2,
            name: 'Удалён'
        }],
        usersActions: ['EditUsers','RemoveUsers','EditGeo','RemoveGeo','EditAdverts','RemoveAdverts','EditClassificator','RemoveClassifications','EditSearchRequest','RemoveSearchRequest']
    }

    var STATUS_CODES = { ACTIVE: 0, BLOCKED: 1, REMOVED: 2 };

    var self = {
        create: function(object,callback) {
            var newModel = new Model(object);

            async.waterfall([
                function(callback) {
                    newModel.save(callback);
                }
            ],function(err, result){
                if(err)
                    throw new Error(err);

                callback(err,result);
            });
        },
        getByID: function(id,populate,callback) {
            Model.findOne({_id: id}).populate(populate).exec(callback);
        },
        setPassword: function(user,password,callback) {
            if(user._password) {
                user._password.disabled = new Date.now();
                user._password.save(function(err){
                    if(err)
                        throw new Error(err);

                    user._oldPasswords.push(user._password._id);
                });
            }
            user._password = password._id;
            user.save(callback);
        }
    }

    //Public methods
    return {
        create: function(object, callback) {
            var publicMethods = this, password;
            async.waterfall([
                function(callback) { //Создаём пароль
                    publicMethods.createPassword(object, callback);
                },
                function(arg, callback) {//Создаём юзера
                    password = arg;
                    object.status.code = object.status.code.id;
                    self.create(object,callback);
                },
                function(user, callback) {//Прикрепляем юзеру пароль
                    self.setPassword(user,password,callback);
                }
            ],function(err, result) {
                if(err)
                    throw new Error(err);

                callback(err, 'Ok');
            });
        },
        createPassword: function(object, callback) {
            if(object.password && object.reEnterPassword) {
                if(object.password == object.reEnterPassword) {
                    var password = new PasswordModel();
                    password.newPassword = object.password;

                    password.save(function(err){
                        if(err)
                            throw new Error(err);

                        callback(null,password);
                    })
                } else {
                    throw new HttpError('Пароли должны совпадать',406);
                }
            } else {
                throw new HttpError('Заполните поле пароль',406);
            }
        },
        authorization: function(email, password, callback) {
            Model.findOne({ email: email }).populate('_password').exec(function(err, user){
                if (err) { return callback(err); }
                if (!user) {
                    return callback(null, false, { message: t.__('Email или пароль указаны неверно')});
                }
                if (!user._password.checkPassword(password)) {
                    return callback(null, false, { message: t.__('Email или пароль указаны неверно') });
                }
                if (!user.status) {
                    return callback(null, false, { message: t.__('Ваш аккаунт не активирован.') });
                }
                if (user.status.code == STATUS_CODES.REMOVED) {
                    return callback(null, false, { message: t.__('Ваш аккаунт удалён, обратитесь в тех. поддержку.') });
                }
                if (user.status.code == STATUS_CODES.BLOCKED) {
                    return callback(null, false, { message: t.__('Ваш аккаунт заблокирован, обратитесь в тех. поддержку.') });
                }
                return callback(null, user);
            });
        },
        getByID: function(id,populate,callback){
            self.getByID(id,populate,callback);
        },
        getUsersRoles: function() {
            return STATIC.usersRoles;
        },
        getUsersStatuses: function() {
            return STATIC.usersStatuses;
        },
        getUsersActions: function() {
            return STATIC.usersActions;
        }
    }

})();

module.exports = API_user;