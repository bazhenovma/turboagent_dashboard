'use strict';

var Street           = require('application/core/model/geolocation').Street,
    Kladr            = require('kladrapi'),
    _                = require('underscore'),
    async            = require('async'),
    HttpError        = require('application/dependencies/HttpError'),
    crud             = require('./crud');

var API_street = (function(){
    var self = new crud();
    self.staticVars.Model = Street;

    //Public methods
    self.publicMethods.create = function(object,callback) {

        async.waterfall([
            /**
             * Ищем адресс в базе КЛАДР вернётся список найднных обьектов
             * @param callback
             */
            function(callback) {
                self.publicMethods.findInKladr(object,callback);
            },
            function(kladrSearchResults,callback) {
                if (kladrSearchResults.result.length == 0) {
                    throw new HttpError('Такой улицы не существует в базе КЛАДР', 406);
                }
                //Если адресов нашлось много то выбираем тот который совпадает по заданному имени

                var kladrObject = self.selectNeededAddressInKladrResults(kladrSearchResults, object.name.split(' ')[0]);

                _.extend(object, {
                    kladr_ID: kladrObject.id,
                    okato_ID: kladrObject.okato,
                    name: kladrObject.name,
                    typeLongName: kladrObject.type,
                    typeShortName: kladrObject.typeShort,
                    _town: object._town._id
                });

                self.staticVars.geoPoint.createPointForStreet(object._town,object.name,callback);
            }
        ],function(err, point){
            if(err)
                throw new Error(err);

            if(point) {
                object._geopoint = point._id;
                self.privateMethods.create(object,callback);
            }
        });
    };

    self.publicMethods.findInKladr = function(object, callback) {
        Kladr.ApiQuery(
            '536788cefca916e6390e2758',
            '8daf624b1e0966b786f72b07149f18b2ef9dc1c6',
            {
                ContentName: object.name ,
                ContentType:'street',
                ParentType:'city',
                ParentId: object._town.kladr_ID
            },callback);
    }
    return self.publicMethods;
})();

module.exports = API_street;