'use strict';
var async = require('async'),
    _ = require('underscore'),
    HttpError = require('application/dependencies/HttpError');

module.exports = function () {
    var staticVars = {};

    var privateMethods = {
        create: function (object, callback) {
            var newModel = new staticVars.Model(object);

            async.waterfall([
                function (callback) {
                    newModel.save(callback);
                }
            ], function (err, result) {
                callback(err, newModel);
            });
        },
        update: function (id, object, callback) {
            this.getByID(id, function (err, result) {
                if (err)
                    throw new Error(err);

                if (result) {
                    _.extend(result, object);
                    result.save(callback);
                } else
                    throw new HttpError('Не найдено', 404);
            });
        },
        getByID: function (id, populate, callback) {
            if (typeof populate == 'function') {
                callback = populate;
            } else {
                if (populate.length > 0) {
                    return staticVars.Model.findById(id).populate(populate).exec(callback);
                }
            }
            return staticVars.Model.findById(id,callback);
        }
    }

    //Public methods
    var publicMethods = {
        create: function (object, callback) {
            privateMethods.create(object, callback);
        },
        update: function (id, object, callback) {
            privateMethods.update(id, object, callback);
        },
        findOne: function (params, callback) {
            staticVars.Model.findOne(params, callback);
        },
        findAll: function (params, populate, sort, callback) {
            if (typeof populate == 'function') {
                callback = populate;
            } else {
                if (populate.length > 0) {
                    staticVars.Model.find(params).populate(populate).sort(sort).exec(callback);
                    return;
                }
            }
            staticVars.Model.find(params).sort(sort).exec(callback);
        },
        getByID: function (id, populate, callback) {
            privateMethods.getByID(id, populate, callback);
        },
        remove: function (id, callback) {
            this.getByID(id, function (err, result) {
                if (err)
                    throw new Error(err);

                if (result)
                    result.remove(callback);
                else
                    throw new HttpError('Не найдено', 404);
            })
        },
        init: function (geoLocation_api) {
            staticVars.geoPoint = geoLocation_api.geoPoint;
        }
    }

    return {
        staticVars: staticVars,
        publicMethods: publicMethods,
        privateMethods: privateMethods,
        selectNeededAddressInKladrResults: function (kladrSearchResults, neededAddress) {
            var kladrObject = kladrSearchResults.result[0];

            if (kladrSearchResults.result.length >= 1) {
                for (var i = 0, resultLen = kladrSearchResults.result.length; i < resultLen; i++) {
                    if (kladrSearchResults.result[i].name == neededAddress) {
                        kladrObject = kladrSearchResults.result[i];
                        break;
                    }
                }
            }
            return kladrObject;
        }
    }
}