'use strict';

var crud = require('./crud'),
    GeoPoint = require('application/core/model/geolocation').geoPoint,
    http = require('http'),
    async = require('async'),
    HttpError = require('application/dependencies/HttpError');

var API_geoPoint = (function () {
    var self = new crud();
    self.staticVars.Model = GeoPoint;
    self.staticVars.geoCodingService = {
        host: 'http://maps.googleapis.com',
        url: '/maps/api/geocode/json?sensor=false'
    }

    self.privateMethods.create = function (object, callback) {
        var newGeoPoint = new GeoPoint(object);
        async.waterfall([
            //Ищем точку в базе если уже добавлена то возвращаем найденную точку
            function (callback) {
                self.privateMethods.getPointByCoords(object, callback);
            },
            function (arg, callback) {
                if (arg)
                    newGeoPoint = arg;
                else {
                    newGeoPoint.save(callback);
                    return;
                }
                callback();
            }
        ], function (err, result) {
            if (err)
                throw new Error(err);

            callback(null, newGeoPoint);
        });
    };
    /**
     * Получаем координаты с сервиса Google.Map
     * на вход подаём адресную строку 'Россия, Красноярский край, Красноярск'
     * на выходе получаем кординаты если координаты не найдены то влозвращаем в callback null
     *
     * @param address
     * @param callback
     */
    self.privateMethods.getCoords = function (address, callback) {
        var options = {
            host: self.staticVars.geoCodingService.host,
            path: self.staticVars.geoCodingService.url + '&address=' + address,
            agent: false
        };
        var responseData = '';

        var request = http.get(options.host + options.path,function (response) {
            if (response.statusCode == 200) {
                response.setEncoding('utf8');
                response.on("data", function (chunk) {
                    responseData += chunk;
                });
                response.on("end", function () {
                    responseData = JSON.parse(responseData);
                    if(responseData.status !== 'ZERO_RESULTS')
                        callback(null, responseData.results[0].geometry.location);
                    else {
                        callback(new Error('Address not found: '+address), null);
                    }
                });
            } else {
                throw HttpError('Address ' + address + ' not found in ' + options.host, 500);
            }
        }).on('error', function (err) {
            throw new Error(err);
        });
        request.shouldKeepAlive = false
    };

    self.privateMethods.getPointByCoords = function (coords, callback) {
        GeoPoint.findOne(coords, callback);
    };

    //Public methods
    self.publicMethods.createPointForCountry = function (country, callback) {
        self.publicMethods.createPointForAddress(country, callback);
    };
    self.publicMethods.createPointForArea = function (country, area, callback) {
        async.series({
            country: function (callback) {
                self.staticVars.country_api.getByID(country, callback);
            }
        }, function (err, result) {
            if (err)
                throw new Error(err);

            if (result.country) {
                self.publicMethods.createPointForAddress(result.country.name + ', ' + area, callback);
            } else
                throw new HttpError('Country id:' + country + ' not found', 404);
        })
    };

    self.publicMethods.createPointForTown = function (areaID, town, callback) {
        var area = '';

        async.waterfall([
            function (callback) {
                self.staticVars.area_api.getByID(areaID, callback);
            },
            function (arg, callback) {
                area = arg;
                if (area)
                    self.staticVars.country_api.getByID(area._country, callback);
                else
                    throw new Error('Area ' + areaID + ' not found');
            }
        ], function (err, result) {
            if (err)
                throw new Error(err);

            if (result)
                self.publicMethods.createPointForAddress(result.name + ', ' + area.name + ', ' + town, callback);
            else
                throw new HttpError('Area id:' + area.id + ' not attached to country', 500);
        })
    };

    self.publicMethods.createPointForStreet = function (townID, streetName, callback) {
        var town, area;

        async.waterfall([
            function (callback) {
                self.staticVars.town_api.getByID(townID, callback);
            },
            function (arg, callback) {
                town = arg;
                self.staticVars.area_api.getByID(town._area, callback);
            },
            function (arg, callback) {
                area = arg;
                self.staticVars.country_api.getByID(area._country, callback);
            }
        ], function (err, result) {
            if (err)
                throw new Error(err);

            if (result)
                self.publicMethods.createPointForAddress(result.name + ', ' + area.name + ', ' + town.name + ', ' + streetName, callback);
            else
                throw new HttpError('Get address for ' + streetName + ' int town ' + townID + ' failed.', 500);
        })
    };

    self.publicMethods.createPointForDistrict = function (townID, districtName, callback) {
        var town, area;

        async.waterfall([
            function (callback) {
                self.staticVars.town_api.getByID(townID, callback);
            },
            function (arg, callback) {
                town = arg;
                self.staticVars.area_api.getByID(town._area, callback);
            },
            function (arg, callback) {
                area = arg;
                self.staticVars.country_api.getByID(area._country, callback);
            }
        ], function (err, result) {
            if (err)
                throw new Error(err);

            if (result)
                self.publicMethods.createPointForAddress(result.name + ', ' + area.name + ', ' + town.name + ', ' + districtName, callback);
            else
                throw new HttpError('Get address for ' + districtName + ' int town ' + townID + ' failed.', 500);
        })
    };

    self.publicMethods.createPointForHouse = function (house, callback) {
        var town, area, street, district;

        async.waterfall([
            function (callback) {
                if(house._district)
                    self.staticVars.district_api.getByID(house._district._id, callback);
                else
                    callback(null,null);
            },
            function (arg, callback) {
                if(arg)
                    district = arg;
                self.staticVars.street_api.getByID(house._street._id, callback);
            },
            function (arg, callback) {
                street = arg;
                self.staticVars.town_api.getByID(street._town, callback);
            },
            function (arg, callback) {
                town = arg;
                self.staticVars.area_api.getByID(town._area, callback);
            },
            function (arg, callback) {
                area = arg;
                self.staticVars.country_api.getByID(area._country, callback);
            }
        ], function (err, result) {
            if (err)
                throw new Error(err);

            if (result) {
                var shortAddress = (district ? district.name +', ' : '')  + street.typeShortName + '. ' + street.name + ', ' + house.typeShortName + '. ' + house.house;
                var fullAddress = house.zip + ', ' + result.name + ', ' + area.name + ' ' + area.typeShortName.toLowerCase() + ', ' + town.name + ', ' + shortAddress;

                self.publicMethods.createPointForAddress(fullAddress, function(err,point) {
                    callback(err,{point: point,fullAddress : fullAddress, shortAddress: shortAddress});
                });
            }
            else
                throw new HttpError('Get address for ' + house + ' failed.', 500);
        })
    };

    self.publicMethods.createPointForAddress = function (address, callback) {
        self.privateMethods.getCoords(address, function (err, point) {
            if (point) {
                self.privateMethods.create({point: [point.lat,point.lng]}, callback);
            } else
                throw new HttpError('GeoPoint for address:' + address + ' not exists', 500);
        });
    };

    self.publicMethods.init = function (geoLocation_api) {
        self.staticVars.country_api = geoLocation_api.country;
        self.staticVars.area_api = geoLocation_api.area;
        self.staticVars.town_api = geoLocation_api.town;
        self.staticVars.street_api = geoLocation_api.street;
        self.staticVars.district_api = geoLocation_api.district;
    };

    return self.publicMethods;
})();

module.exports = API_geoPoint;