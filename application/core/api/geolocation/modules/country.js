'use strict';

var Country          = require('application/core/model/geolocation').Country,
    crud             = require('./crud');

var API_country = (function(){

    var self = new crud();
    self.staticVars.Model = Country;

    //Public methods
    self.publicMethods.create = function(object,callback) {
        self.staticVars.geoPoint.createPointForCountry(object.name,function(err, point){
            if(err)
                throw new Error(err);

            if(point) {
                object._geopoint = point._id;
                self.privateMethods.create(object,callback);
            }
        });
    };
    return self.publicMethods;
})();

module.exports = API_country;