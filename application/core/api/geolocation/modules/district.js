'use strict';

var District         = require('application/core/model/geolocation').District,
    async            = require('async'),
    HttpError        = require('application/dependencies/HttpError'),
    crud             = require('./crud');

var API_district = (function(){

    var self = new crud();
    self.staticVars.Model = District;

    //Public methods
    self.publicMethods.create = function(object,callback) {
        async.waterfall([
            function(callback) {
                object._town = object._town._id;
                self.staticVars.geoPoint.createPointForDistrict(object._town,object.name,callback);
            }
        ],function(err,point){
            if(err)
                throw new Error(err);

            if(point) {
                object._geopoint = point._id;
                self.privateMethods.create(object,callback);
            }
        });
    }
    return self.publicMethods;

})();

module.exports = API_district;