'use strict';

var crud = require('./crud'),
    Kladr = require('kladrapi'),
    _ = require('underscore'),
    async = require('async'),
    Area = require('application/core/model/geolocation').Area,
    HttpError = require('application/dependencies/HttpError');

var API_area = (function () {
    var self = new crud();

    self.staticVars.Model = Area;
    //Public methods
    self.publicMethods.create = function (object, callback) {

        async.waterfall([
            /**
             * Ищем адресс в базе КЛАДР вернётся список найднных обьектов
             * @param callback
             */
            function (callback) {
                self.publicMethods.findInKladr(object, callback);
            },
            function (kladrSearchResults, callback) {
                if(kladrSearchResults.result.length == 0) {
                    throw new HttpError('Такой области не существует в базе КЛАДР',406);
                }
                //Если адресов нашлось много то выбираем тот который совпадает по заданному имени
                var kladrObject = self.selectNeededAddressInKladrResults(kladrSearchResults, object.name.split(' ')[0]);

                _.extend(object, {
                    kladr_ID: kladrObject.id,
                    okato_ID: kladrObject.okato,
                    name: kladrObject.name,
                    typeLongName: kladrObject.type,
                    typeShortName: kladrObject.typeShort
                });

                //Получаем координаты адреса от гугла
                self.staticVars.geoPoint.createPointForArea(object._country, object.name + ' ' + object.typeLongName.toLowerCase(), callback);
            }],
            function (err, point) {
                if (err)
                    throw new Error(err);

                if (point) {
                    object._geopoint = point._id;
                    self.privateMethods.create(object, callback);
                }
            });
    };

    self.publicMethods.getByID = function (id, callback) {
        self.privateMethods.getByID(id, '_country', callback);
    };

    /**
     * Поис по базе КЛАДР передаём обьект который содержит имя региона
     *
     * @param object
     * @param callback
     */
    self.publicMethods.findInKladr = function (object, callback) {
        Kladr.ApiQuery(
            '536788cefca916e6390e2758',
            '8daf624b1e0966b786f72b07149f18b2ef9dc1c6',
            {
                ContentName: object.name.split(' ')[0],
                WithParent: false,
                Limit: 2,
                ContentType: 'region'
            }, callback);
    }
    return self.publicMethods;
})();

module.exports = API_area;