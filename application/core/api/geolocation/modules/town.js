'use strict';

var Town = require('application/core/model/geolocation').Town,
    Kladr = require('kladrapi'),
    _ = require('underscore'),
    async = require('async'),
    HttpError = require('application/dependencies/HttpError'),
    crud = require('./crud');

var API_town = (function () {
    var self = new crud();
    self.staticVars.Model = Town;

    //Public methods
    self.publicMethods.create = function (object, callback) {

        async.waterfall([
            /**
             * Ищем адресс в базе КЛАДР вернётся список найднных обьектов
             * @param callback
             */
            function (callback) {
                self.publicMethods.findInKladr(object, callback);
            },
            function (kladrSearchResults, callback) {
                if (kladrSearchResults.result.length == 0) {
                    throw new HttpError('Такого города не существует в базе КЛАДР', 406);
                }
                //Если адресов нашлось много то выбираем тот который совпадает по заданному имени
                var kladrObject = self.selectNeededAddressInKladrResults(kladrSearchResults, object.name);

                _.extend(object, {
                    kladr_ID: kladrObject.id,
                    okato_ID: kladrObject.okato,
                    name: kladrObject.name,
                    typeLongName: kladrObject.type,
                    typeShortName: kladrObject.typeShort,
                    _area: object._area._id
                });
                self.staticVars.geoPoint.createPointForTown(object._area, object.name, callback);
            }
        ], function (err, point) {
            if (err)
                throw new Error(err);

            if (point) {
                object._geopoint = point._id;
                self.privateMethods.create(object, callback);
            }
        });
    };

    self.publicMethods.findInKladr = function (object, callback) {
        Kladr.ApiQuery(
            '536788cefca916e6390e2758',
            '8daf624b1e0966b786f72b07149f18b2ef9dc1c6',
            {
                ContentName: object.name,
                ContentType: 'city',
                ParentType: 'region',
                ParentId: object._area.kladr_ID
            }, callback);
    }
    return self.publicMethods;

})();

module.exports = API_town;