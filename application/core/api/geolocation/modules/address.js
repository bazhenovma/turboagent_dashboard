'use strict';

var Address = require('application/core/model/geolocation').Address,
    crud = require('./crud'),
    _ = require('underscore'),
    async = require('async'),
    Kladr = require('kladrapi'),
    domain              = require('domain'),
    HttpError = require('application/dependencies/HttpError');

var API_address = (function () {
    var self = new crud();

    self.staticVars.Model = Address;
    //Public methods
    self.publicMethods.create = function (object, callback, handleHttp) {

        async.waterfall([
            function (callback) {
                self.publicMethods.findOne({house: object.house, _street: object._street._id},callback);
            },
            /**
             * Ищем адресс в базе КЛАДР вернётся список найднных обьектов
             * @param callback
             */
            function (findResult,callback) {
                if(findResult) {
                    throw new HttpError('Такой адрес уже есть в базе', 406);
                }
                self.publicMethods.findInKladr(object, callback);
            },
            function (kladrSearchResults, callback) {
                if (kladrSearchResults.result.length == 0) {
                    throw new HttpError('Такого адреса не существует в базе КЛАДР', 406);
                }
                //Если адресов нашлось много то выбираем тот который совпадает по заданному имени
                var kladrObject = self.selectNeededAddressInKladrResults(kladrSearchResults, object.house + (object.housing ? '/' + object.housing : ''));

                _.extend(object, {
                    kladr_ID: kladrObject.id,
                    okato_ID: kladrObject.okato,
                    house: kladrObject.name,
                    zip: kladrObject.zip,
                    typeLongName: kladrObject.type,
                    typeShortName: kladrObject.typeShort
                });

                self.staticVars.geoPoint.createPointForHouse(object, callback);
            }
        ], function (err, result) {
            if(handleHttp && err instanceof HttpError) {
                callback(err);
            }

            if (err)
                throw new Error(err);

            if (result) {
                _.extend(object, {
                    fullAddress: result.fullAddress,
                    shortAddress: result.shortAddress,
                    _geopoint: result.point._id,
                    _street: object._street._id
                });

                self.privateMethods.create(object, callback);
            }
        })
    };

    self.publicMethods.findInKladr = function (object, callback) {
        Kladr.ApiQuery(
            '536788cefca916e6390e2758',
            '8daf624b1e0966b786f72b07149f18b2ef9dc1c6',
            {
                ContentName: object.house + (object.housing ? '/' + object.housing : ''),
                ContentType: 'building',
                ParentType: 'street',
                ParentId: object._street.kladr_ID
            }, callback);
    };

    self.publicMethods.update = function (id, object, callback) {
        self.staticVars.geoPoint.createPointForHouse(object, function (point, fullAddress) {
            if (point) {
                object.full = fullAddress;
                object._geopoint = point._id;
                self.privateMethods.update(id, object, callback);
            }
        });
    };

    /**
     * Полнотекстовый поиск по базе для MongoDB v > 2.6
     * Ищем по полю ShortAddress содрежащий только район, улицу, дом
     *
     * @param data
     * @param callback
     */
    self.publicMethods.findByShortStringAndTown = function (data, callback) {
        self.staticVars.Model
            .find({$text: { $search: data.text }, _town: data._town})
            .populate('_district _street _town')
            .exec(callback);
    };


    /**
     *
     */
    self.publicMethods.loadAllAddressesFromStreet = function(street, callback) {
        var count = 0, noEmptyResult = true, addedAddresses = 0;

        async.whilst(
            function () { return noEmptyResult; },
            function (callback) {
                count++;
                Kladr.ApiQuery(
                    '536788cefca916e6390e2758',
                    '8daf624b1e0966b786f72b07149f18b2ef9dc1c6',
                    {
                        ContentName: count,
                        ContentType: 'building',
                        ParentType: 'street',
                        ParentId: street.kladr_ID,
                        limit:1000
                    }, function(err,adresses){
                        if(err)
                            throw new Error(err);

                        if (adresses.result.length == 0) {
                            noEmptyResult = false;
                        }
                        createAddressesFromKladrResult(street,adresses.result, function(added){
                            addedAddresses += added;
                            callback();
                        });
                    });
            },
            function (err) {
                if(err)
                    throw new Error(err);

                callback();
            }
        );
    }

    function createAddressesFromKladrResult(street, addresses, callback) {
        var currentAddress = 0, addedAddresses = 0;
        var d = domain.create();

        d.on('error', function(err) {
            callback();
        });

        d.run(function () {
            async.whilst(
                function () { return currentAddress < addresses.length; },
                function (callback) {
                    self.publicMethods.create({
                        house: addresses[currentAddress].name,
                        _street: street,
                        _district: street._district,
                        _town: street._town
                    }, function(err, result){
                        if(err) {
                            console.log(err);
                        } else {
                            if(result)
                                addedAddresses++;
                        }
                        currentAddress++;
                        callback();
                    },true);
                },
                function (err) {
                    if(err)
                        throw new Error(err);

                    callback(addedAddresses);
                }
            );
        });

    }

    return self.publicMethods;
})();

module.exports = API_address;