'use strict';
var _ = require('underscore');

module.exports = function(){

    var modules = {
        address:    require('./modules/address'),
        area:       require('./modules/area'),
        country:    require('./modules/country'),
        district:   require('./modules/district'),
        geoPoint:   require('./modules/geopoint'),
        street:     require('./modules/street'),
        town:       require('./modules/town')
    };

    return {
        init: function() {
            for(var key in modules) {
                if(typeof modules[key] == 'object') {
                    modules[key].init(modules);
                }
            }
            return modules;
        }
    }
};