'use strict';

module.exports = (function(){
    return {
        api: require('./api'),
        security: require('./security')
    }
})();