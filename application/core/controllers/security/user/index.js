'use strict';

var controller = new require('application/core/controllers/controller')(),
    api = require('application/core/api/user/');

var nav_controller = (function(){

    return controller.extend({
        actions: {
            getUsersRoles: function(req,res) {
                var result = api.getUsersRoles();
                res.json(200,result);
            },
            getUsersActions: function(req,res) {
                var result = api.getUsersActions();
                res.json(200,result);
            },
            getUsersStatuses: function(req,res) {
                var result = api.getUsersStatuses();
                res.json(200,result);
            }
        },
        accessRules: {
            allow: {
                actions : ['getUsersRoles','getUsersActions','getUsersStatuses'],
                roles: ['*'],
                users: ['*']
            }
        }
    });

})();

module.exports = nav_controller;