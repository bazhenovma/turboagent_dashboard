'use strict';

var controller = new require('../../controller')(),
    geoLocation_api  = new require('application/core/api/geolocation/')().init(),
    api      = geoLocation_api.country;

var search_controller = (function(){

    return controller.extend({
        actions: {
            get: function(req,res) {
                api.getByID(req.params.id,function(err, result){
                    controller.handleError(err);
                    res.json(result);
                });
            },
            getAll: function(req,res) {
                api.findAll(null,function(err, result){
                    controller.handleError(err);
                    res.json(result);
                });
            },
            add: function(req,res,next) {
                api.create(req.body,function(err, result){
                    controller.handleError(err);
                    res.json(result);
                });
            },
            update: function(req,res,next) {
                api.update(req.body._id, req.body.data,function(err, result){
                    controller.handleError(err);
                    res.json(result);
                });
            },
            remove: function(req,res) {
                api.remove(req.body._id,function(err, result){
                    controller.handleError(err);
                    res.json(200,'Ok');
                });
            }
        },
        accessRules: [
            {
                actions : ['get','add','remove','getAll','update'],
                roles: ['*'],
                users: ['*']
            }
        ]
    });

})();

module.exports = search_controller;