
module.exports = {
    address: require('./address'),
    area: require('./area'),
    country: require('./country'),
    district: require('./district'),
    street: require('./street'),
    town: require('./town')
}