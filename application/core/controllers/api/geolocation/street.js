'use strict';

var controller = new require('../../controller')(),
    geoLocation_api  = new require('application/core/api/geolocation/')().init(),
    HttpError   = require('application/dependencies/HttpError'),
    api_address  = geoLocation_api.address,
    api      = geoLocation_api.street;

var search_controller = (function(){

    return controller.extend({
        actions: {
            get: function(req,res) {
                api.getByID(req.params.id,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            add: function(req,res) {
                if(req.body._district)
                    req.body._district = req.body._district._id;
                else
                    throw new HttpError('Не указан район',406);

                api.create(req.body,function(err, result){
                    if(err)
                        throw new Error(err);

                    api_address.loadAllAddressesFromStreet(result,function(err){
                        if(err)
                            throw new Error(err);

                        res.json(result);
                    });
                });
            },
            update: function(req,res) {
                api.update(req.body._id, req.body.data,function(err, result){
                    if(err)
                        throw new Error(err);

                    api_address.loadAllAddressesFromStreet(result,function(err){
                        if(err)
                            throw new Error(err);

                        res.json(result);
                    });
                });
            },
            remove: function(req,res) {
                api.remove(req.body._id,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(200,'Ok');
                });
            },
            getAll: function(req,res) {
                api.findAll({_street: req.params.street},'_town _district',{house: 1},function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            }
        },
        accessRules: [
            {
                actions : ['get','add','remove','update','getAll'],
                roles: ['*'],
                users: ['*']
            }
        ]
    });

})();

module.exports = search_controller;