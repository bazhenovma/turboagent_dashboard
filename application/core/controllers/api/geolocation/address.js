'use strict';

var controller = new require('../../controller')(),
    geoLocation_api  = new require('application/core/api/geolocation/')().init(),
    api      = geoLocation_api.address;

var search_controller = (function(){

    return controller.extend({
        actions: {
            get: function(req,res) {
                api.getByID(req.params.id,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            find: function(req,res) {
                api.findByShortStringAndTown(req.body,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            update: function(req,res) {
                api.update(req.body._id, req.body.data,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            add: function(req,res) {
                api.create(req.body,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            remove: function(req,res) {
                api.remove(req.body._id,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(200,'Ok');
                });
            },
            getAll: function(req,res) {
                api.findAll({_street: req.params.street},'_street _district _town',{house: 1},function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            }
        },
        accessRules: [
            {
                actions : ['get','add','remove','find','update','getAll'],
                roles: ['*'],
                users: ['*']
            }
        ]
    });

})();

module.exports = search_controller;