'use strict';

var controller          = new require('../../controller')(),
    async               = require('async'),
    api_advert          = require('application/core/api/advert'),
    api_house           = require('application/core/api/house'),
    api_object          = require('application/core/api/object');

var advert_controller = (function(){

    return controller.extend({
        actions: {
            add: function(req,res) {
                async.waterfall([
                    function(callback) {
                        var house = req.body.house;
                        if(house._id) {
                            return callback(null, house);
                        }
                        house._address = house._address._id;
                        house.gas = req.body.object ? 1 : 0;
                        house.status = { code: api_house.STATUS_CODE.addedByUser, _codeChanger: req.user._id };
                        house._author = req.user._id;

                        api_house.create(house,callback);
                    },
                    function(house, callback) {
                        var object = req.body.object;
                        object._house = house._id;
                        object.gas = object.gas ? 1 : 0;

                        api_object.create(object,callback);
                    },
                    function(object, callback) {
                        var advert = req.body.advert;
                        advert._house = object._house;
                        advert._object = object._id;
                        advert._user = req.user._id;
                        advert.status = {code : api_advert.STATUS_CODE.published,_codeChanger : req.user._id};

                        if(object._images.length > 0)
                            advert.photo = true;

                        api_advert.create(advert,callback);
                    }
                ],function(err, result) {
                    if(err)
                        throw new Error(err);

                    if(result)
                        res.json(result);
                });
            }
        },
        accessRules: [
            {
                actions : ['add'],
                users: ['@']
            }
        ]
    });

})();

module.exports = advert_controller;