'use strict';

var controller = new require('application/core/controllers/controller')(),
    api = require('application/core/api/nav/');

var nav_controller = (function(){

    return controller.extend({
        actions: {
            getPrimaryNav: function(req,res) {
                var result = api.getPrimaryNavForCurrentUser(req);
                res.json(200,result);
            }
        },
        accessRules: [
            {
                actions : ['getPrimaryNav'],
                roles: ['*'],
                users: ['*']
            }
        ]
    });

})();

module.exports = nav_controller;