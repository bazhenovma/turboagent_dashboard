'use strict';

var controller  = new require('../../controller')(),
    _           = require('underscore'),
    api         = require('application/core/api/user');

var user_controller = (function(){

    return controller.extend({
        actions: {
            get: function(req,res) {
                api.getByID(req.params.id,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            getCurrent: function(req,res) {
                res.json(_.pick(req.user,'email','info','purse'));
            },
            add: function(req,res) {
                api.create(req.body,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            update: function(req,res) {
                api.update(req.body._id, req.body.data,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json(result);
                });
            },
            remove: function(req,res) {
                api.remove(req.body._id,function(err, result){
                    if(err)
                        throw new Error(err);

                    res.json('Ok');
                });
            },
            preAuthorization: function(req,res,next) {
                next();
            },
            afterAuthorization: function(req,res,next) {
                var token = req.user.encodeToken(req.ip);
                res.json({id: req.user._id, role: req.user.access.role, token: token});
             },
            loadToken: function(req,res) {
                if(req.user) {
                    res.json({id: req.user._id, role: req.user.access.role, token: req.body.token});
                } else
                    res.json(401,'Token not correctly');
            }
        },
        accessRules: [
            {
                actions : ['get','add','remove','update','getAll','loadToken'],
                users: ['*']
            },
            {
                actions: ['logout','afterAuthorization','getCurrent'],
                users: ['@']
            },
            {
                actions: ['preAuthorization'],
                users: ['~']
            }
        ]
    });

})();

module.exports = user_controller;