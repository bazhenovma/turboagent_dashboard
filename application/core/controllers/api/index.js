'use strict';

module.exports = {
    nav: require('./nav'),
    geolocation: require('./geolocation'),
    advert: require('./advert'),
    user: require('./user')
}