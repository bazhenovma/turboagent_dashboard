'use strict';

var _           = require('underscore'),
    async       = require('async'),
    decodeToken = require('application/core/model/user').decodeToken,
    user_API    = require('application/core/api/user'),
    emitter     = require('events').EventEmitter(),
    HttpError   = require('application/dependencies/HttpError'),
    domain      = require('domain');

var controller = function() {

    var self = {
        init : function() {

        },
        actions : {},
        accessRules : {},
        handleError: function(err) {
            if(err) {
                var errors = {};
                if(err.message == 'Validation failed') {
                    for(var fieldError in err.errors) {
                        errors[fieldError] = {type: 'unique', val: false};
                    }
                    throw new HttpError('Ошибка валидации',406,errors);
                }
                throw new Error(err);
            }
        },
        runAction : function(action) {
            return function(req,res,next) {
                try {
                    if(typeof self.actions[action] == 'undefined') {
                        throw new HttpError('Страница не найдена',404);
                    }
                    async.waterfall([
                        function(callback){
                            self.beforeAction(action,req,callback)
                        },
                        function(callback) {
                            self.actions[action](req,res,next);
                            callback();
                        },
                        function(callback) {
                            self.afterAction(action,req,callback);
                        }],
                        function(err,results){
                            if(err) throw err;
                        });
                } catch(e) {
                    return domain.active.emit('error', e);
                }
            }
        },
        beforeAction : function(action,req,callback) {

            /**
             * Авторизуем юзера по токену
             */
            if(req.headers['x-user-token'] || req.body.token) {
                var decoded = decodeToken(req.headers['x-user-token'] ? req.headers['x-user-token'] : req.body.token);

                if(Date.now() > decoded.expired) {
                    return callback(new HttpError('Токен истёк',403));
                }
                if(req.ip != decoded.ip) {
                    return callback(new HttpError('Это не ваш токен',403));
                }
                user_API.getByID(decoded.id,'',function(err,user){
                    if(err)
                        throw  new Error(err);

                    if(user) {
                        req.user = user;
                        self.checkAccess(action,req,callback);
                    } else
                        return callback(new HttpError('Не правильный токен',403));
                });
            } else
                self.checkAccess(action,req,callback);

            //emitter.emit('beforeAction',action,req,res);
        },
        afterAction : function(action,req, callback) {
            callback();
            //emitter.emit('afterAction',action,req,res)
        },
        checkAccess: function(action,req,callback) {
            var current_action_rules;

            /**
             *  Получаем правила дступа для текущего action
             */
            for(var index in self.accessRules) {
                if(self.accessRules[index].actions.indexOf(action) != -1) {
                    current_action_rules = {
                        users: self.accessRules[index].users ? self.accessRules[index].users : [],
                        roles: self.accessRules[index].roles ? self.accessRules[index].roles : []
                    };
                }
            }

            //Проверяем открыт ли доступ к action
            if(!current_action_rules)
                return callback(new HttpError('Доступ запрёщен',403));

            //Если открыт проверяем всем ли юзерам
            if(current_action_rules.users.indexOf('*') != -1)
                return callback(null);

            //Если не открыт всем юзерам проверяем авторизацию
            if(current_action_rules.users.indexOf('@') != -1) {
                if(!req.user)
                    return callback(new HttpError('Доступ запрёщен, только для авторизованных пользователей',403));
                else
                    return callback(null);
            }

            //Если открыт только авторизананным проверяем авторизацию
            if(current_action_rules.users.indexOf('~') != -1) {
                if(req.user)
                    return callback(new HttpError('Доступ запрёщен, только для неавторизованных пользователей',403));
                else
                    return callback(null);
            }

            //Если не открыта текущему юзеру
            if(req.user)
                if(current_action_rules.users.indexOf(req.user.login) != -1) {
                    //Если не открыт всем юзерам и авторизованным проверяем роль
                    if(current_action_rules.roles.indexOf(req.user.access.role) != -1) {
                        return callback(null);
                    } else {
                        return callback(new HttpError('Доступ запрёщен',403));
                    }
                }

            return callback(new HttpError('Доступ запрёщен',403));
        },
        extend: function(source) {
            return _.extend(self,source);
        }
    };

    return self;
};

module.exports = controller;