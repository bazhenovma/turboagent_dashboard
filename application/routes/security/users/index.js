'use strict';

module.exports = function(router,controllers) {
    router.get('/security/dashboard/get/users/roles/',controllers.runAction('getUsersRoles'));
    router.get('/security/dashboard/get/users/actions/',controllers.runAction('getUsersActions'));
    router.get('/security/dashboard/get/users/statuses/',controllers.runAction('getUsersStatuses'));
}