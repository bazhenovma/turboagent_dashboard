'use strict';
var passport = require('passport');

module.exports = function(router,controller) {
    router.get('/api/dashboard/get/user/:id/',controller.runAction('get'));
    router.get('/api/user/getCurrent/',controller.runAction('getCurrent'));
    router.post('/api/dashboard/add/user/',controller.runAction('add'));
    router.post('/api/dashboard/update/user/',controller.runAction('update'));
    router.post('/api/dashboard/remove/user/',controller.runAction('remove'));
    router.post('/api/login/',controller.runAction('preAuthorization'),passport.authenticate('local',{session: false}),controller.runAction('afterAuthorization'));
    router.post('/api/load/token/',controller.runAction('loadToken'));
}