'use strict';

module.exports = function(router,controllers) {
    require('./advert')(router,controllers.advert);
    require('./nav')(router,controllers.nav);
    require('./geolocation')(router,controllers.geolocation);
    require('./user')(router,controllers.user);
}