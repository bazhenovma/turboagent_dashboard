'use strict';

module.exports = function(router,controllers) {
    router.get('/api/dashboard/get/country/:id/',controllers.country.runAction('get'));
    router.get('/api/dashboard/get/all/countries/',controllers.country.runAction('getAll'));
    router.post('/api/dashboard/add/country/',controllers.country.runAction('add'));
    router.post('/api/dashboard/update/country/',controllers.country.runAction('update'));
    router.post('/api/dashboard/remove/country/',controllers.country.runAction('remove'));

    router.get('/api/dashboard/get/area/:id/',controllers.area.runAction('get'));
    router.post('/api/dashboard/add/area/',controllers.area.runAction('add'));
    router.get('/api/dashboard/get/all/areas/:country/',controllers.area.runAction('getAll'));
    router.post('/api/dashboard/update/area/',controllers.area.runAction('update'));
    router.post('/api/dashboard/remove/area/',controllers.area.runAction('remove'));

    router.get('/api/dashboard/get/town/:id/',controllers.town.runAction('get'));
    router.post('/api/dashboard/add/town/',controllers.town.runAction('add'));
    router.get('/api/dashboard/get/all/towns/:area/',controllers.town.runAction('getAll'));
    router.post('/api/dashboard/update/town/',controllers.town.runAction('update'));
    router.post('/api/dashboard/remove/town/',controllers.town.runAction('remove'));

    router.get('/api/dashboard/get/district/:id/',controllers.district.runAction('get'));
    router.post('/api/dashboard/add/district/',controllers.district.runAction('add'));
    router.get('/api/dashboard/get/all/districts/:town/',controllers.district.runAction('getAll'));
    router.post('/api/dashboard/update/district/',controllers.district.runAction('update'));
    router.post('/api/dashboard/remove/district/',controllers.district.runAction('remove'));

    router.get('/api/dashboard/get/street/:id/',controllers.street.runAction('get'));
    router.post('/api/dashboard/add/street/',controllers.street.runAction('add'));
    router.get('/api/dashboard/get/all/streets/:town/',controllers.street.runAction('getAll'));
    router.post('/api/dashboard/update/street/',controllers.street.runAction('update'));
    router.post('/api/dashboard/remove/street/',controllers.street.runAction('remove'));

    router.get('/api/dashboard/get/address/:id/',controllers.address.runAction('get'));
    router.post('/api/dashboard/add/address/',controllers.address.runAction('add'));
    router.post('/api/dashboard/update/address/',controllers.address.runAction('update'));
    router.get('/api/dashboard/get/all/addresses/:street/',controllers.address.runAction('getAll'));
    router.post('/api/dashboard/remove/address/',controllers.address.runAction('remove'));
    router.post('/api/dashboard/find/address/',controllers.address.runAction('find'));
}