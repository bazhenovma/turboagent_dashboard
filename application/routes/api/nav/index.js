'use strict';

module.exports = function(router,controllers) {
    router.get('/api/dashboard/get/nav/primary/',controllers.runAction('getPrimaryNav'));
}