'use strict';

var controllers    = require('../core/controllers'),
    path           = require('path');

module.exports = function() {
    return {
        init: function(router){
            require('./api')(router, controllers.api);
            require('./security')(router, controllers.security);

            router.all('*',function(req,res){
                if (res.req.headers['x-requested-with'] == 'XMLHttpRequest') {
                    res.json(404,'Page not found');
                } else {
                    res.sendfile(path.join(__dirname,'/../../../../app/frontend/pages/dashboard/index.html'));
                }
            });
        }
    }
}